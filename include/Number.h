#pragma once

namespace StrongTyped {

/*
	CRTP Class for wrapping some entities to provide strong type safety.
	Designed to be a wrapper for some ID types, doesn't have modifying methods.

	Usage:
		// can be forwarded
		struct MyEntityId;
		struct MyEntityId : public StrongTyped::Value<size_t, MyEntityId> {};
		std::map<MyEntityId, std::unique_ptr<MyEntity> > entities;

		// can be specified a custom default value (initially is 0)
		struct Id : public StrongTyped::Value<int, Id> { static Type Default() { return -1; }; };

		// instances are to be constructed via static method
		auto my_id = Id::Construct(23);
*/
template <class TypeT, typename SelfType> class Value {
protected:
	TypeT val;
public:
	Value() : val(SelfType::Default()) {};

	typedef TypeT Type; // for external access
	static SelfType Construct(const TypeT& v) { SelfType result; result.val = v; return result; };
	static TypeT Default() { return 0; };

	TypeT value() const { return val; };

	bool operator <(const SelfType& right) const { return val < right.val; };
	bool operator <=(const SelfType& right) const { return val <= right.val; };
	bool operator >(const SelfType& right) const { return val > right.val; };
	bool operator >=(const SelfType& right) const { return val >= right.val; };

	bool operator ==(const SelfType& right) const { return val == right.val; };
	bool operator !=(const SelfType& right) const { return val != right.val; };
};

/*
	Math-oriented class expanding Value, implements arithmetic operations.
*/
template <class TypeT, typename SelfType> class Number : public Value<TypeT, SelfType> {
public:
	using Value<TypeT, SelfType>::value;
	TypeT& value() { return this->val; };

	SelfType operator +(const SelfType& right) const { return this->Construct(this->val + right.val); };
	SelfType operator -(const SelfType& right) const { return this->Construct(this->val - right.val); };
	TypeT operator /(const SelfType& right) const { return this->val / right.val; };

	SelfType& operator +=(const SelfType& right) { this->val += right.val; return *static_cast<SelfType*>(this); };
	SelfType& operator -=(const SelfType& right) { this->val -= right.val; return *static_cast<SelfType*>(this); };

	SelfType operator *(const TypeT& multiplier) const { return this->Construct(this->val * multiplier); };
	SelfType operator /(const TypeT& divider) const { return this->Construct(this->val / divider); };

	SelfType& operator *=(const TypeT& multiplier) { this->val *= multiplier; return *static_cast<SelfType*>(this); };
	SelfType& operator /=(const TypeT& divider) { this->val /= divider; return *static_cast<SelfType*>(this); };
};

/*
	Extension for those entities, that can be multiplied or divided with each other.
*/
template <class TypeT, typename SelfType> struct NumberEx : public Number<TypeT, SelfType> {
	SelfType operator *(const SelfType& right) const { return this->Construct(this->val * right.value()); };
	SelfType operator /(const SelfType& right) const { return this->Construct(this->val / right.value()); };
	SelfType& operator *=(const SelfType& right) { this->val *= right.val; return *static_cast<SelfType*>(this); };
	SelfType& operator /=(const SelfType& right) { this->val /= right.val; return *static_cast<SelfType*>(this); };
};

}; // end namespace StrongTyped
