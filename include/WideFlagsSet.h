#pragma once
#include <bitset>
#include <cassert>
#include <iterator>

namespace StrongTyped {

/*
	Compact bit storage for strong typed flags (enum class) with comfortable operations and iteration.
	Usage:
		enum class Foo : int {
			F1,
			F2,
			F3,
			F4,
			F5,
			TOTAL_COUNT // last value
		};
		typedef StrongTyped::WideFlagsSet<Foo, unsigned(Foo::TOTAL_COUNT)> FooFlags;

		bool doF1() { return true; };

		bool f() {
			FooFlags fl;
			fl.set({Foo::F1, Foo::F3, Foo::F4, Foo::F5}).reset({Foo::F2, Foo::F4});
			for(auto f : fl) {
				// ...
			}
			auto to_do = fl.copy().setIf(Foo::F2, fl.isAny()).reset(Foo::F5);

			return to_do.testNot(Foo::F1) || (doF1() && to_do.reset(Foo::F1).asTrue());
		};
*/
template <class EnumClass, unsigned FlagsCount> class WideFlagsSet {
	std::bitset<FlagsCount> bits;

	class Iterator;
	WideFlagsSet(const decltype(bits)& b) : bits(b) {};
public:
	WideFlagsSet() {};
	WideFlagsSet(EnumClass flag) {
		if(unsigned(flag) < bits.size()) {
			bits.set(unsigned(flag));
		}
	};
	WideFlagsSet(std::initializer_list<EnumClass> flags) {
		for(auto flag : flags) {
			if(unsigned(flag) < bits.size()) {
				bits.set(unsigned(flag));
			}
		}
	};
	WideFlagsSet& set(const WideFlagsSet& other) { bits |= other.bits; return *this; };
	WideFlagsSet& reset(const WideFlagsSet& other) { bits &= ~other.bits; return *this; };
	WideFlagsSet& setIf(const WideFlagsSet& other, bool condition) { return condition ? set(other) : *this; };
	WideFlagsSet& resetIf(const WideFlagsSet& other, bool condition) { return condition ? reset(other) : *this; };
	WideFlagsSet& setTo(const WideFlagsSet& other, bool state) { return state ? set(other) : reset(other); };
	WideFlagsSet& set() { bits.set(); return *this; };
	WideFlagsSet& reset() { bits.reset(); return *this; };
	WideFlagsSet& invert() { bits.flip(); return *this; };
	WideFlagsSet copy() const { return *this; };
	WideFlagsSet united(const WideFlagsSet& other) const { return bits | other.bits; };
	WideFlagsSet intersection(const WideFlagsSet& other) const { return bits & other.bits; };
	WideFlagsSet difference(const WideFlagsSet& other) const { return bits & ~other.bits; };
	WideFlagsSet symmetricDifference(const WideFlagsSet& other) const { return bits ^ other.bits; };
	WideFlagsSet negation() const { return ~bits; };
	unsigned count() const { return bits.count(); };
	bool asTrue() const { return true; };
	bool isNone() const { return bits.none(); };
	bool isAny() const { return bits.any(); };
	bool isAll() const { return bits.all(); };
	bool test(const WideFlagsSet& other) const { auto b = (bits & other.bits); return (b == other.bits) && b.any(); };
	bool testNot(const WideFlagsSet& other) const { return ! test(other); };
	bool testAnyOf(const WideFlagsSet& other) const { return (bits & other.bits).any(); };
	bool testNoneOf(const WideFlagsSet& other) const { return ! testAnyOf(other); };
	bool operator ==(const WideFlagsSet& other) const { return bits == other.bits; };
	bool operator !=(const WideFlagsSet& other) const { return ! operator==(other); };

	Iterator begin() const { return Iterator(this, true); };
	Iterator end() const { return Iterator(this, false); };

private:
	class Iterator : public std::iterator<std::random_access_iterator_tag, EnumClass> {
		const WideFlagsSet* owner;
		unsigned pos;

		void next() {
			if(pos != FlagsCount) {
				++pos;
			}
			while(pos != FlagsCount) {
				if(owner->bits[pos]) break;
				++pos;
			}
		};
	public:
		Iterator(const WideFlagsSet* _owner, bool is_first) : owner(_owner), pos(is_first ? -1 : FlagsCount) { if(is_first) next(); };
		void operator ++() { next(); };
		EnumClass operator*() const { return static_cast<EnumClass>(pos); };
		bool operator !=(const Iterator& other) const { assert(owner == other.owner); return pos != other.pos; };
	};
};

}; // end namespace StrongTyped
