#pragma once
#include "../include/Number.h"

#include <type_traits>

namespace StrongTyped {

namespace Local {

template <class T> struct ScalableTypeBase {
	typedef T ValueType;
	static ValueType Multiplier() { return 1; };
};

template <typename First, typename Second, typename Base>
struct HeirsOf : public std::integral_constant<bool, std::is_base_of<Base, First>::value && std::is_base_of<Base, Second>::value> {};

template <typename First, typename Second, typename Base1, typename Base2, typename Base3>
struct HeirsOfAny : public std::integral_constant<bool,
	HeirsOf<First, Second, Base1>::value ||
	HeirsOf<First, Second, Base2>::value ||
	HeirsOf<First, Second, Base3>::value> {};

template <typename First, typename Second, typename... Bases>
struct DifferentHeirsOfAny : public std::integral_constant<bool,
	HeirsOfAny<First, Second, Bases...>::value && (! std::is_same<First, Second>::value)> {};

}; // end namespace Local

// distance types
struct DistanceT : public Local::ScalableTypeBase<double> { protected: DistanceT() {}; };
struct DistanceMeters : public Number<DistanceT::ValueType, DistanceMeters>, public DistanceT {
	static ValueType Multiplier() { return 100; }; };
struct DistanceKiloMeters : public Number<DistanceT::ValueType, DistanceKiloMeters>, public DistanceT {
	static ValueType Multiplier() { return 1000 * 100; }; };
struct DistanceMiles : public Number<DistanceT::ValueType, DistanceMiles>, public DistanceT {
	static ValueType Multiplier() { return 1.6 * 1000 * 100; }; };

// duration types
struct DurationT : public Local::ScalableTypeBase<double> { protected: DurationT() {}; };
struct DurationSeconds : public Number<DurationT::ValueType, DurationSeconds>, public DurationT {
	static ValueType Multiplier() { return 1; }; };
struct DurationMinutes : public Number<DurationT::ValueType, DurationMinutes>, public DurationT {
	static ValueType Multiplier() { return 60; }; };
struct DurationHours : public Number<DurationT::ValueType, DurationHours>, public DurationT {
	static ValueType Multiplier() { return 60 * 60; }; };

// speed types
struct SpeedT : public Local::ScalableTypeBase<double> { protected: SpeedT() {}; };
struct SpeedKMH : public Number<SpeedT::ValueType, SpeedKMH>, public SpeedT {
	static ValueType Multiplier() { return DistanceKiloMeters::Multiplier() / DurationHours::Multiplier(); }; };
struct SpeedMPH : public Number<SpeedT::ValueType, SpeedMPH>, public SpeedT {
	static ValueType Multiplier() { return DistanceMiles::Multiplier() / DurationHours::Multiplier(); }; };

// calculator
template <typename SpeedType, typename DistanceType, typename DurationType>
inline SpeedType CalculateSpeed(const DistanceType& dist, const DurationType& duration) {
	static_assert(std::is_base_of<DistanceT, DistanceType>::value, "DistanceType must be subclass of DistanceT");
	static_assert(std::is_base_of<DurationT, DurationType>::value, "DurationType must be subclass of DurationT");
	static_assert(std::is_base_of<SpeedT, SpeedType>::value, "SpeedType must be subclass of SpeedT");

	return SpeedType::Construct(
		(dist.value() / duration.value()) // the following 3 constants are hopefully optimized by compiler
		* (DistanceType::Multiplier() / DurationType::Multiplier())
		/ SpeedType::Multiplier());
};

// casts
template <typename ResultType, typename OriginalType>
inline ResultType Convert(const OriginalType& original) {
	static_assert(Local::DifferentHeirsOfAny<ResultType, OriginalType, DistanceT, DurationT, SpeedT>::value,
					"Converted value must have same underlying class with result value");
	return ResultType::Construct(original.value() * OriginalType::Multiplier() / ResultType::Multiplier());
};

// custom modifiers
template <typename LeftType, typename RightType,
	typename = typename std::enable_if<Local::DifferentHeirsOfAny<LeftType, RightType, DistanceT, DurationT, SpeedT>::value>::type>
inline LeftType operator +(const LeftType& left, const RightType& right) {
	return LeftType::Construct(left.value() + right.value() * RightType::Multiplier() / LeftType::Multiplier());
};

template <typename LeftType, typename RightType,
	typename = typename std::enable_if<Local::DifferentHeirsOfAny<LeftType, RightType, DistanceT, DurationT, SpeedT>::value>::type>
inline LeftType operator -(const LeftType& left, const RightType& right) {
	return LeftType::Construct(left.value() - right.value() * RightType::Multiplier() / LeftType::Multiplier());
};

template <typename LeftType, typename RightType,
	typename = typename std::enable_if<Local::DifferentHeirsOfAny<LeftType, RightType, DistanceT, DurationT, SpeedT>::value>::type>
inline LeftType& operator +=(LeftType& left, const RightType& right) {
	left.value() += right.value() * RightType::Multiplier() / LeftType::Multiplier();
	return left;
};

template <typename LeftType, typename RightType,
	typename = typename std::enable_if<Local::DifferentHeirsOfAny<LeftType, RightType, DistanceT, DurationT, SpeedT>::value>::type>
inline LeftType& operator -=(LeftType& left, const RightType& right) {
	left.value() -= right.value() * RightType::Multiplier() / LeftType::Multiplier();
	return left;
};

/*
	Sample usage:
		// create
		auto hours = DurationHours::Construct(2);
		auto meters = DistanceMeters::Construct(100000);
		auto miles = DistanceMiles::Construct(10);
		// calculate
		auto kmh = CalculateSpeed<SpeedKMH>(meters, hours);
		kmh = CalculateSpeed<SpeedKMH>(miles, hours / 10);
		auto mph = CalculateSpeed<SpeedMPH>(miles, hours) * 10;
		// modify
		mph *= 5;
		hours += DurationMinutes::Construct(7); // translates minutes to hours inside
		// convert
		auto seconds = Convert<DurationSeconds>(hours); // translates hours to seconds
		hours = hours - seconds; // translates seconds to hours inside, calculates correctly
		hours += seconds; // calculates correctly
*/

}; // end namespace StrongTyped
